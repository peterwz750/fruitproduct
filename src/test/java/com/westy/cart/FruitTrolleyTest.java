package com.westy.cart;

import static org.junit.Assert.*;


import org.junit.Test;

public class FruitTrolleyTest {

	
	@Test
	public void testTotalCost() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 Double expected = 2.55;
		 assertEquals(expected,trolley.TotalCost("Apple,Apple,Orange,Apple,Orange,Orange"));

	}
	
	@Test
	public void testTotalCostOranges() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 Double expected = 1.25;
		 assertEquals(expected,trolley.TotalCost("Orange,Orange,Orange,Orange,Orange"));
		
	}
		 
	@Test
	public void testTotalApples() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 Double expected = 2.40;
		 assertEquals(expected,trolley.TotalCost("Apple,Apple,Apple,Apple"));
		
	}

	@Test
	public void testTotalCostNoFruit() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 Double expected = 0.00;
		 assertEquals(expected,trolley.TotalCost(""));
	}
	
	@Test
	public void testTotalCostNullFruit() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 Double expected = 0.00;
		 assertEquals(expected,trolley.TotalCost(null));
		
	}
	
	@Test
	public void testTotalCostOnOffer() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 trolley.setOnOffer(true);
		 Double expected = 1.70;
		 assertEquals(expected,trolley.TotalCost("Apple,Apple,Orange,Apple,Orange,Orange"));

	}
	
	@Test
	public void testTotalCostOrangesOnOffer() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 trolley.setOnOffer(true);
		 Double expected = 1.00;
		 assertEquals(expected,trolley.TotalCost("Orange,Orange,Orange,Orange,Orange"));
		
	}
		 
	@Test
	public void testTotalApplesOnOffer() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 trolley.setOnOffer(true);
		 Double expected = 1.20;
		 assertEquals(expected,trolley.TotalCost("Apple,Apple,Apple,Apple"));
		
	}
	
	@Test
	public void testTotalCostOrangesBelowOffer() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 trolley.setOnOffer(true);
		 Double expected = 0.50;
		 assertEquals(expected,trolley.TotalCost("Orange,Orange"));
		
	}
	@Test
	public void testTotalApplesOnOffer2() {
		
		 FruitTrolley trolley = new FruitTrolley();
		 trolley.setOnOffer(true);
		 Double expected = 0.60;
		 assertEquals(expected,trolley.TotalCost("Apple"));
		
	}
	
}

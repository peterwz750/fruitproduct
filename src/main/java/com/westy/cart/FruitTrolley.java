package com.westy.cart;



public class FruitTrolley {
	
	private boolean onOffer = false;
	
	public boolean isOnOffer() {
		return onOffer;
	}

	public void setOnOffer(boolean onOffer) {
		this.onOffer = onOffer;
	}


	/*
	 *  method to calculate total cost of fruits
	 *  param - format is comma separated list
	 *  e.g.   "Apple,Apple,Orange,Apple,Orange" 
	 */
	public Double TotalCost(String fruits) {

		Double result = 0.00;

		//ToDo.. add validation here on fruits parameter
		// - check for not null,
		//not zero length and contains valid products etc..
		//otherwise throw exception.. 

		// just for this exercise hard code price - normally subject to lookup 
		final double orangePrice = 0.25;
		final double applePrice = 0.60;	
		
		int appleQuantity = getQuantity("Apple",fruits);
		int orangeQuantity = getQuantity("Orange",fruits);

		if (onOffer) {

			// apples - Buy One get one free / Oranges - 3 for price of 2
			result = calcOffer(appleQuantity,applePrice,applePrice, 2 );
			result = result + calcOffer(orangeQuantity,orangePrice,orangePrice*2,3);

		} else {
			result = (appleQuantity * applePrice) + (orangeQuantity * orangePrice);
		}
		return result;
	}
	
	private Double calcOffer(int quantity, double unitprice, double offerprice, int amount) {
		
		Double result = 0.00;
		
		int offer = quantity/amount;
		int remainder  = quantity % amount;
		
		result = (offer * offerprice) + (remainder * unitprice);
		
		return result;
	}

	private int getQuantity(String product, String list) {
		
		return list.split(product, -1).length-1;
	}
	
}
